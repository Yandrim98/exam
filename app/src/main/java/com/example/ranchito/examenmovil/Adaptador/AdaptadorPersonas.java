package com.example.ranchito.examenmovil.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ranchito.examenmovil.Modelo.Personas;
import com.example.ranchito.examenmovil.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorPersonas extends RecyclerView.Adapter<AdaptadorPersonas.MyViewHolder> {
    private ArrayList<Personas> personas;

    public AdaptadorPersonas(ArrayList<Personas> personas){
        this.personas = personas;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_usuarios, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        Personas personas1 = personas.get(position);
        myViewHolder.ide.setText(personas1.getId());
        myViewHolder.descr.setText(personas1.getDescripcion());
        myViewHolder.parcialone.setText(personas1.getParcial_uno());
        myViewHolder.parcialtow.setText(personas1.getParcial_dos());
        Picasso.get().load(personas1.getImagen()).into(myViewHolder.foto);

    }

    @Override
    public int getItemCount() {
        return personas.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView foto;
        private TextView ide,descr, parcialone,parcialtow;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            foto = (ImageView)itemView.findViewById(R.id.Imagen);
            ide = (TextView)itemView.findViewById(R.id.ide);
            descr= (TextView)itemView.findViewById(R.id.descripia);
            parcialone = (TextView)itemView.findViewById(R.id.parcialuno);
            parcialtow = (TextView)itemView.findViewById(R.id.parcialdos);

        }
    }
}
