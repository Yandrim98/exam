package com.example.ranchito.examenmovil.Modelo;

public class Personas {
  private  String id;
  private  String descripcion;
    private  String parcial_uno;
    private  String parcial_dos;
    private  String imagen;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public void setParcial_uno(String parcial_uno) {
        this.parcial_uno = parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public void setParcial_dos(String parcial_dos) {
        this.parcial_dos = parcial_dos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
