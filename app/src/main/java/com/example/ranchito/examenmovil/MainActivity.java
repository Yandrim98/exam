package com.example.ranchito.examenmovil;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ranchito.examenmovil.Adaptador.AdaptadorPersonas;
import com.example.ranchito.examenmovil.Modelo.Personas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    //Cambia esa url por la que esta en la hoja

    private RecyclerView recyclerView;
    private static final String URL_PRODUCTOS = "https://jsonplaceholder.typicode.com/comments";
    private RecyclerView.LayoutManager layoutManager;
    private AdaptadorPersonas adaptadorPersonas;
    private ArrayList<Personas> arrayListP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.Persona);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListP = new ArrayList<>();
        adaptadorPersonas = new AdaptadorPersonas(arrayListP);
        progressDialog = new ProgressDialog(this);

        ObtenerProductos();
    }
    private void ObtenerProductos(){
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Log.e("Mensaje", jsonObject.toString());
                        Personas personas = new Personas();

                       personas.setId(jsonObject.getString("id"));
                        personas.setDescripcion(jsonObject.getString("descripcion"));
                        personas.setParcial_uno(jsonObject.getString("parcial_uno"));
                        personas.setParcial_dos(jsonObject.getString("parcial_dos"));
                        personas.setImagen(jsonObject.getString("imagen"));
                        jsonObject.getString("name");

                        arrayListP.add(personas);

                    }
                    recyclerView.setAdapter(adaptadorPersonas);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
